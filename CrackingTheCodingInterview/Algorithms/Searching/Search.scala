package CrackingTheCodingInterview.Algorithms.Searching

import CrackingTheCodingInterview.Commons.Printer

import scala.collection.mutable.ArrayBuffer

abstract class Search extends Printer {

  def searchArray(array: ArrayBuffer[Int], value: Int): Int
}
