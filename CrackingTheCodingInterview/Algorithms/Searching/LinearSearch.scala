package CrackingTheCodingInterview.Algorithms.Searching

import scala.collection.mutable.ArrayBuffer

/* ***************************************

0) Nazwa:

Linear search - Przeszukiwanie liniowe

1) Zlozonosc obliczeniowa:

LinearSearch posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n);
 - Avg case to O(n/2);
 - Best case to O(1).

LinearSearch posiada zlozonosc pamieciowa (space complexity) O(1)

2) Przyklad uzycia:

val LinearSearcher = new LinearSearch()
val value = LinearSearcher.searchArray(ArrayBuffer[Int](2, 3, 14, 12, 3, 132, 34), 34)
println(s"The result value of linear search is $value")

3) Opis:

 *************************************** */

class LinearSearch extends Search {

  def searchArray(array: ArrayBuffer[Int], value: Int): Int = array.filter(item => item.equals(value)).head

}
