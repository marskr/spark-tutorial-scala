package CrackingTheCodingInterview.Algorithms.Sorting

import scala.collection.mutable.ArrayBuffer

/* ***************************************

0) Nazwa:

Merge sort - Sortowanie przez scalanie

1) Zlozonosc obliczeniowa:

MergeSort posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n*log(n));
 - Avg case to O(n*log(n));
 - Best case to O(n*log(n)).

MergeSort posiada zlozonosc pamieciowa O(n)

2) Przyklad uzycia:

val notSortedArray = ArrayBuffer[Int](2, 3, 14, 12, 3, -132, 34)
val MergeSorter = new MergeSort()
MergeSorter.printArray(MergeSorter.sortArray(notSortedArray))

3) Opis:

Sortowanie poprzez scalanie - dzielenie tablicy a potem scalanie w
odpowiedniej kolejnosci

 *************************************** */

class MergeSort extends Sort {

  override def sortArray(notSortedArray: ArrayBuffer[Int]): ArrayBuffer[Int] = ???

}
