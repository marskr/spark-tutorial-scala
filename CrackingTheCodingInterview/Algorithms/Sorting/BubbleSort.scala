package CrackingTheCodingInterview.Algorithms.Sorting

import scala.collection.mutable.ArrayBuffer

/* ***************************************

0) Nazwa:

Bubble sort/Swap sort - Sortowanie babelkowe

1) Zlozonosc obliczeniowa:

BubbleSort posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n^2) porownan, O(n^2) zamian;
 - Avg case to O(n^2) porownan, O(n^2) zamian;
 - Best case to O(n) porownan, O(1) zamian.

BubbleSort posiada zlozonosc pamieciowa (space complexity) O(1)

2) Przyklad uzycia:

val notSortedArray = ArrayBuffer[Int](2, 3, 14, 12, 3, -132, 34)
val BubbleSorter = new BubbleSort()
BubbleSorter.printArray(BubbleSorter.sortArray(notSortedArray))

3) Opis:

Sortowanie poprzez podmiane elementow tablicy, jesli sa w niezgodnej z
modelem sortowania kolejnosci

 *************************************** */

class BubbleSort extends Sort {

  override def sortArray(notSortedArray: ArrayBuffer[Int]): ArrayBuffer[Int] = {
    var isAnyMoved = false
    do {
      isAnyMoved = false
      for (i <- 0 to (notSortedArray.length - 2)) {
        if (notSortedArray(i) > notSortedArray(i + 1)) {
          val tmp = notSortedArray(i)
          notSortedArray(i) = notSortedArray(i + 1)
          notSortedArray(i + 1) = tmp
          isAnyMoved = true
        }
      }
    } while (isAnyMoved.equals(true))
    val sortedArray = notSortedArray
    sortedArray
  }

}
