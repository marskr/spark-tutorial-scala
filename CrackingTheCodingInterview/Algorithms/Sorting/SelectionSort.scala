package CrackingTheCodingInterview.Algorithms.Sorting

import scala.collection.mutable.ArrayBuffer

/* ***************************************

0) Nazwa:

Selection sort - Sortowanie przez wybieranie

1) Zlozonosc obliczeniowa:

SelectionSort posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n^2) porownan, O(n) zamian;
 - Avg case to O(n^2) porownan, O(n) zamian;
 - Best case to O(n^2) porownan, O(1) zamian.

SelectionSort posiada zlozonosc pamieciowa (space complexity) O(1)

2) Przyklad uzycia:

val notSortedArray = ArrayBuffer[Int](2, 3, 14, 12, 3, -132, 34)
val sorter = new CrackingTheCodingInterview.Algorithms.Sorting.SelectionSorter()
sorter.PrintArray(sorter.SortArray(notSortedArray))

3) Opis:

Sortowanie poprzez wybor maksymalnego/minimalnego elementu i
przesuniecia go na kraniec listy (i tak iterujac po calej liscie)

 *************************************** */

class SelectionSort extends Sort {

  override def sortArray(notSortedArray: ArrayBuffer[Int]): ArrayBuffer[Int] = {
    val sortedArray = ArrayBuffer[Int]()
    var extreme: Int = 0
    while(notSortedArray.nonEmpty) {
      extreme = notSortedArray.head
      notSortedArray.foreach{ item => if (item < extreme) extreme = item } // znalezienie najmniejszego elementu
      sortedArray += extreme
      notSortedArray -= extreme
    }
    sortedArray
  }

}
