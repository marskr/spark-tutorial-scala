package CrackingTheCodingInterview.Algorithms.Sorting

import scala.collection.mutable.ArrayBuffer

/* ***************************************

0) Nazwa:

Quick sort - Sortowanie szybkie

1) Zlozonosc obliczeniowa:

QuickSort posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n^2);
 - Avg case to O(n*log(n)).

QuickSort posiada zlozonosc pamieciowa zalezna od implementacji

2) Przyklad uzycia:

val notSortedArray = ArrayBuffer[Int](2, 3, 14, 12, 3, -132, 34)
val QuickSorter = new QuickSort()
QuickSorter.printArray(QuickSorter.sortArray(notSortedArray))

3) Opis:

Sortowanie poprzez podzial tabeli wedle wybranej losowo wartosci,
nastepnie rekursywne sortowanie lewej i prawej czesci (analogicznie
kolejnymi podzialami)

 *************************************** */

class QuickSort extends Sort {

  override def sortArray(notSortedArray: ArrayBuffer[Int]): ArrayBuffer[Int] = {
    if (notSortedArray.length <= 1) notSortedArray
    else {
      val divisionValue = notSortedArray(notSortedArray.length/2)
      val sortedArray = sortArray(notSortedArray.filter(a => divisionValue > a)) ++
        notSortedArray.filter(a => a.equals(divisionValue)) ++
        sortArray(notSortedArray.filter(a => divisionValue < a))
      sortedArray
    }
  }

}
