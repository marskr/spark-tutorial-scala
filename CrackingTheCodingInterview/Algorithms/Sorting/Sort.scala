package CrackingTheCodingInterview.Algorithms.Sorting

import CrackingTheCodingInterview.Commons.Printer

import scala.collection.mutable.ArrayBuffer

abstract class Sort extends Printer {

  def sortArray(notSortedArray: ArrayBuffer[Int]): ArrayBuffer[Int]
}
