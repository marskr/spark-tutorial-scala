package CrackingTheCodingInterview.Algorithms.Trees.BinaryTree

case class Leaf[T](v: T) extends BinaryTree[T]
