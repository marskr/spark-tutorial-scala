package CrackingTheCodingInterview.Algorithms.Trees.BinaryTree

import scala.annotation.tailrec

/* ***************************************

0) Nazwa:

Binary tree - Drzewo binarne

1) Zlozonosc obliczeniowa:

Dla binarnego drzewa poszukiwan (wydajnosc, performance):
 - Worst case to O(n);
 - Best case to O(log(n)).

2) Przyklad uzycia:

val tree = Node(1, Node(2, Leaf(4), Leaf(5)), Node(3, Leaf(6), Empty))
println(tree.depthFirstSearch(6)())

3) Opis:

 *************************************** */

trait BinaryTree[+T] {
  def value: Option[T] = this match {
    case n: Node[T] => Some(n.v)
    case l: Leaf[T] => Some(l.v)
    case Empty => None
  }

  def left: Option[BinaryTree[T]] = this match {
    case n: Node[T] => Some(n.l)
    case _ => None
  }

  def right: Option[BinaryTree[T]] = this match {
    case n: Node[T] => Some(n.r)
    case _ => None
  }

  // depth-first search
  // wyszukiwanie DFS - schodzenie w pionie (eksploracja calych galezi kompletnie nastepnie przejscie do kolejnej)
  // nalezy zapamietywac nieodwiedzone wierzcholki, aby moc odwiedzic je pozniej
  def depthFirstSearch[T](t: T)(f: T => Boolean = (x: T) => x == t)(implicit tree: BinaryTree[T] = this): Option[BinaryTree[T]] = {
    @tailrec
    def search(subTree: BinaryTree[T], remainder: List[BinaryTree[T]]): Option[BinaryTree[T]] = {
      subTree match {
        case Empty => if (remainder.isEmpty) None else search(remainder.head, remainder.tail)
        case Leaf(v) => if (f(v)) Some(Leaf(v)) else if (remainder.isEmpty) None else search(remainder.head, remainder.tail)
        case Node(v, l, r) => if (f(v)) Some(Node(v, l, r)) else search(l, r :: remainder)
      }
    }
    search(tree, List())
  }

  // width-first search


}
