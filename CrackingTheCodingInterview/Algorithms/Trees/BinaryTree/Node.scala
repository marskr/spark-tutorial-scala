package CrackingTheCodingInterview.Algorithms.Trees.BinaryTree

case class Node[T](v: T, l: BinaryTree[T], r: BinaryTree[T]) extends BinaryTree[T]
