package CrackingTheCodingInterview.Commons

import scala.collection.mutable.ArrayBuffer

trait Printer {

  def printArray(array: ArrayBuffer[Int]): Unit = println(array.mkString(", "))
}
