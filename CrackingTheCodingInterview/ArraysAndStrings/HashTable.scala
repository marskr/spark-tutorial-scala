package CrackingTheCodingInterview.ArraysAndStrings

import scala.collection.mutable

/* ***************************************

0) Nazwa:

Hash table - Tablica mieszajaca

1) Zlozonosc obliczeniowa:

HashTable posiada zlozonosc obliczeniowa (wydajnosc, performance):
 - Worst case to O(n);
 - Avg case to O(1).

HashTable posiada zlozonosc pamieciowa (space complexity) O(1)

2) Przyklad uzycia:

val moduloValue = 20
val hashMap = new mutable.HashMap[Int, Int]
val hashTable = new HashTable(hashMap, moduloValue)

hashTable.insert(22, 20)
hashTable.insert(23, 24)

hashTable.nicePrint(4)
hashTable.nicePrint(2)

3) Opis:

HashTable to struktura danych, ktora mapuje klucze z wartosciami. Pozwala na efektywny lookup wartosci

 *************************************** */

class HashTable[A](hashMap: mutable.HashMap[Int, A], moduloValue: Int) {

  var hashTable: mutable.HashMap[Int, A] = hashMap

  def insert(key: Int, value: A): mutable.Map[Int, A] = hashTable += (hashCode(key) -> value)

  def get(key: Int): A = hashTable(key)

  def nicePrint(key: Int): Unit = hashTable.get(key) match {
    case None => println(s"There is no value for key $key in HashTable")
    case Some(value) => println(s"HashTable result for key $key is $value")
  }

  private def hashCode(key : Int): Int = key%moduloValue
}
