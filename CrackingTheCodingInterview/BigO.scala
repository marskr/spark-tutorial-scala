package CrackingTheCodingInterview

import scala.collection.mutable.ArrayBuffer

abstract class AbstractBigO {

  def O_N_runtime (Array: ArrayBuffer[Int])

  def O_N_N_runtime (Array: ArrayBuffer[Int])

  def O_A_B_runtime (ArrayA: ArrayBuffer[Int], ArrayB: ArrayBuffer[Int])
}

class BigO extends AbstractBigO {

  // Annotation: this will take O(N) time. The fact that we iterate through the array twice doesn't metter
  //    var array = ArrayBuffer[Int](2, 3, 14, 12, 3, 132, 34)
  //    val runtime = new BigO()
  //    runtime.O_N_runtime(array)
  //    runtime.O_N_N_runtime(array)
  //    runtime.O_A_B_runtime(array, array)
  def O_N_runtime (Array: ArrayBuffer[Int]): Unit ={

    var sum = 0
    var product = 1

    Array.foreach(sum += _)
    Array.foreach(product *= _)

    println("Suma jako: " + sum)
    println("Multiplikacja jako: " + product)

    (sum, product)
  }

  // Annotation: this will take O(N*N) time. The fact that we iterate through the array twice doesn't metter
  //    var array = ArrayBuffer[Int](2, 3, 14, 12, 3, 132, 34)
  //    val runtime = new BigO()
  //    runtime.O_N_runtime(array)
  //    runtime.O_N_N_runtime(array)
  //    runtime.O_A_B_runtime(array, array)
  def O_N_N_runtime (Array: ArrayBuffer[Int]): Unit ={

    var sum = 0
    var product = 1

    for (x <- Array){

      Array.foreach(sum += _)
      sum += x

      Array.foreach(product *= _)
      product *= x
    }

    println("Sum as: " + sum)
    println("Multiplication as: " + product)

    (sum, product)
  }

  // Annotation: this will take O(A*B) time.
  //    var array = ArrayBuffer[Int](2, 3, 14, 12, 3, 132, 34)
  //    val runtime = new BigO()
  //    runtime.O_N_runtime(array)
  //    runtime.O_N_N_runtime(array)
  //    runtime.O_A_B_runtime(array, array)
  def O_A_B_runtime (ArrayA: ArrayBuffer[Int], ArrayB: ArrayBuffer[Int]): Unit ={

    var sum = 0

    for (x <- ArrayA){

      for (y <- ArrayB){

        sum += x
        sum += y
      }
    }

    print(s"Sum as: $sum")

    sum
  }
}
