package CrackingTheCodingInterview.Recursion

import scala.annotation.tailrec

/* ***************************************

0) Nazwa:

Recurrence & Tail recurrence - Rekurencja i rekurencja ogonowa

1) Zlozonosc obliczeniowa:

2) Przyklad uzycia:

println(s"basic recursion sum result is ${BasicAndTailRecursion.basicRecursionSum(List(-1, -2, -3, -4, 1253, -2))}")
println(s"tail recursion sum result is ${BasicAndTailRecursion.tailRecursionSum(List(-1, -2, -3, -4, 1253, -2))}")
println(s"basic recursion max result is ${BasicAndTailRecursion.basicRecursionMax(List(-1, -2, -3, -4, 1253, -2))}")
println(s"tail recursion max result is ${BasicAndTailRecursion.tailRecursionMax(List(-1, -2, -3, -4, 1253, -2))}")
println(s"basic recursion min result is ${BasicAndTailRecursion.basicRecursionMin(List(-1, -2, -3, -4, 1253, -2))}")
println(s"tail recursion min result is ${BasicAndTailRecursion.tailRecursionMin(List(-1, -2, -3, -4, 1253, -2))}")

3) Opis:

 *************************************** */

object BasicAndTailRecursion {

  def basicRecursionSum(list: List[Int]): Int = list match {
    case Nil => 0
    case item :: tail => item + basicRecursionSum(tail)
  }

  def tailRecursionSum(list: List[Int]): Int = {
    @tailrec
    def sum(list: List[Int], accumulator: Int): Int = {
      list match {
        case Nil => accumulator
        case item :: tail => sum(tail, accumulator + item)
      }
    }
    sum(list, 0)
  }

  def basicRecursionMax(list: List[Int]): Int = list match {
    case Nil => -2147483648
    case item :: tail => if (item > basicRecursionMax(tail)) item else basicRecursionMax(tail)
  }

  def tailRecursionMax(list: List[Int]): Int = {
    @tailrec
    def max(list: List[Int], accumulator: Int): Int = {
      list match {
        case Nil => accumulator
        case item :: tail => if (item > accumulator) max(tail, item) else max(tail, accumulator)
      }
    }
    max(list, -2147483648)
  }

  def basicRecursionMin(list: List[Int]): Int = list match {
    case Nil => 2147483647
    case item :: tail => if (item < basicRecursionMin(tail)) item else basicRecursionMin(tail)
  }

  def tailRecursionMin(list: List[Int]): Int = {
    @tailrec
    def min(list: List[Int], accumulator: Int): Int = {
      list match {
        case Nil => accumulator
        case item :: tail => if (item < accumulator) min(tail, item) else min(tail, accumulator)
      }
    }
    min(list, 2147483647)
  }

}
