package CrackingTheCodingInterview.Buffer

/* ***************************************

0) Nazwa:

Last In First Out\Stack - Stos

1) Zlozonosc obliczeniowa:

2) Przyklad uzycia:

val buffer = List(1, 3)
val lifo = new LIFO[Int](buffer)
lifo.add(35)
lifo.add(3)
lifo.remove()
println(lifo.buffer)
println(lifo.isEmpty)
lifo.remove()
lifo.remove()
lifo.remove()
println(lifo.isEmpty)

3) Opis:

 *************************************** */

class LIFO[T](arg: List[T]) extends Buffer(arg) {

  def add(item: T): Unit = buffer = item :: buffer

  def remove(): Unit = {
    buffer = buffer match {
      case List() => List()
      case _ :: tail => tail
    }
  }
}
