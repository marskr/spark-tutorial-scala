package CrackingTheCodingInterview.Buffer

/* ***************************************

0) Nazwa:

Last In First Out\Stack - Stos

1) Zlozonosc obliczeniowa:

2) Przyklad uzycia:

val buffer = List(1, 3)
val fifo = new FIFO[Int](buffer)
fifo.add(35)
fifo.add(3)
fifo.remove()
println(fifo.buffer)
println(fifo.isEmpty)
fifo.remove()
fifo.remove()
fifo.remove()
println(fifo.isEmpty)

3) Opis:

 *************************************** */

class FIFO[T](arg: List[T]) extends Buffer(arg) {

  def add(item: T): Unit = buffer = item :: buffer

  def remove(): Unit = {
    buffer = buffer match {
      case List() => List()
      case a => a.take(a.length - 1)
    }
  }
}
