package CrackingTheCodingInterview.Buffer

abstract class Buffer[T](arg: List[T]) {

  var buffer: List[T] = arg

  def add(item: T): Unit

  def remove(): Unit

  def peek(): T = buffer.head

  def isEmpty: Boolean = buffer.isEmpty
}
